package com.example.testapplication.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testapplication.R;
import com.example.testapplication.adapters.TimeListAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvTimerList;
    TimeListAdapter timeListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init recyclerview
        rvTimerList = findViewById(R.id.rvTimerList);

        //init adapter
        timeListAdapter = new TimeListAdapter();

        //set adapter
        rvTimerList.setAdapter(timeListAdapter);
        rvTimerList.setLayoutManager(new LinearLayoutManager(this));
    }
}
