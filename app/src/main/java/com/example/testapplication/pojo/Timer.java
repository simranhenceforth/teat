package com.example.testapplication.pojo;

import java.sql.Time;
import java.util.Date;
import android.os.Handler;

public class Timer {

    long lastTimerStoped, startTime=0, time;
    int timerIncremenrBy=1, timer=0;
    Handler handler;
    Runnable runnable;

    public Runnable getRunnable() {
        return runnable;
    }

    public void setRunnable(Runnable runnable) {
        this.runnable = runnable;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public int getTimerIncremenrBy() {
        return timerIncremenrBy;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public void setTimerIncremenrBy(int timerIncremenrBy) {
        this.timerIncremenrBy = timerIncremenrBy;
    }

    public long getLastTimerStoped() {
        return lastTimerStoped;
    }

    public void setLastTimerStoped(long lastTimerStoped) {
        this.lastTimerStoped = lastTimerStoped;
    }
}
