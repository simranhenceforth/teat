package com.example.testapplication.adapters;

import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testapplication.R;
import com.example.testapplication.pojo.Timer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TimeListAdapter extends RecyclerView.Adapter<TimeListAdapter.ViewHolder> {
    List<Timer> timerList = new ArrayList<>();

    public TimeListAdapter() {
        for (int i = 0; i < 50; i++) {
            Timer timer = new Timer();
            timer.setLastTimerStoped(0);
            timer.setTimerIncremenrBy(1);
            timerList.add(timer);
        }
    }

    @NonNull
    @Override
    public TimeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate
                (R.layout.timer_item_view, parent, false));
    }

    public void startTimer(final int position) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long sec = timerList.get(position).getTime() + (timerList.get(position).getTimerIncremenrBy() * 1000);
                timerList.get(position).setTime(sec);
                notifyItemChanged(position, "timer");
                handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(runnable, 1000);
        timerList.get(position).setHandler(handler);
        timerList.get(position).setRunnable(runnable);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.size() != 0 && payloads.get(0).equals("timer")) {
            if (timerList.get(position).getTime() != null) {
                long milliseconds = timerList.get(position).getTime();
                int seconds = (int) (milliseconds / 1000) % 60;
                int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
                int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

                String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
                holder.tvTimerText.setText(timeString);
            }
        } else
            super.onBindViewHolder(holder, position, payloads);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull TimeListAdapter.ViewHolder holder, int position) {
        if (timerList.get(position).getTime() != null) {
            long milliseconds = timerList.get(position).getTime();
            int seconds = (int) (milliseconds / 1000) % 60;
            int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
            int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

            String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
            holder.tvTimerText.setText(timeString);
        }
    }

    @Override
    public int getItemCount() {
        return 50;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvAdd, tvMinus, tvText;
        TextView tvTimerText;
        Button btnAction, btnStop;

        public ViewHolder(View itemView) {
            super(itemView);

            //init views
            tvTimerText = itemView.findViewById(R.id.tvTimerText);
            btnAction = itemView.findViewById(R.id.btnAction);
            btnStop = itemView.findViewById(R.id.btnStop);
            tvMinus = itemView.findViewById(R.id.tvMinus);
            tvAdd = itemView.findViewById(R.id.tvAdd);
            tvText = itemView.findViewById(R.id.tvText);

            tvMinus.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View v) {
                    int number = Integer.parseInt(tvText.getText().toString());
                    if (number > -10) {
                        tvText.setText(String.valueOf(number - 1));
                        timerList.get(getAdapterPosition()).setTimerIncremenrBy(number - 1);
                    }
                }
            });

            tvAdd.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View v) {
                    int number = Integer.parseInt(tvText.getText().toString());
                    if (number < 10) {
                        tvText.setText(String.valueOf(number + 1));
                        timerList.get(getAdapterPosition()).setTimerIncremenrBy(number + 1);
                    }
                }
            });

            // click on stop button
            btnStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //reset timer and stop
                    timerList.get(getAdapterPosition()).setTime(0L);
                    timerList.get(getAdapterPosition()).getHandler().removeCallbacks(
                            timerList.get(getAdapterPosition()).getRunnable()
                    );

                    notifyItemChanged(getAdapterPosition(),"timer");
                    btnAction.setText(R.string.start);
                }
            });

            // click on start/pause/resume button
            btnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // if start button
                    if (btnAction.getText().equals("Start")) {
                        timerList.get(getAdapterPosition()).setTime(0L);
                        timerList.get(getAdapterPosition()).setStartTime(Calendar.getInstance().getTime().getTime());
                        startTimer(getAdapterPosition());
                        btnAction.setText(R.string.pause);
                        // click on pause button
                    } else if (btnAction.getText().equals("Pause")) {
                        timerList.get(getAdapterPosition()).getHandler().removeCallbacks(timerList.get(getAdapterPosition()).getRunnable());
                        btnAction.setText(R.string.resume);
                        // click on resume button
                    } else if (btnAction.getText().equals("Resume")) {
                       timerList.get(getAdapterPosition()).getHandler().postDelayed(timerList.get(getAdapterPosition()).getRunnable(), 1000);
                        btnAction.setText(R.string.pause);
                    }
                }
            });
        }
    }
}